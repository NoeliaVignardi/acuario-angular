import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PecesService } from '../servicios/peces.service';
import { ProductosService } from '../servicios/productos.service';
import { Pez } from '../models/pez.model';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html'
})
export class BuscadorComponent implements OnInit {

  peces:any[] = []
  productos:any[] = []
  termino: string;

  constructor( private activatedRoute:ActivatedRoute,
               private _PecesService: PecesService,
               private _ProductosService: ProductosService) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe (params => {
      this.termino = params['termino'];
      this.peces = this._PecesService.buscarPeces( params['termino'])
      this.productos = this._ProductosService.buscarProductos( params['termino'])
      //console.log(this.peces && this.productos);
    })
  }

}
