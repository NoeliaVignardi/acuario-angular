import { Component, OnInit } from '@angular/core';
import { PecesComponent } from '../peces/peces.component';
import { Pez } from '../models/pez.model';
import { PecesService } from '../servicios/peces.service';

@Component({
  selector: 'app-agregar-pez',
  templateUrl: './agregar-pez.component.html',
  styleUrls: ['./agregar-pez.component.css']
})
export class AgregarPezComponent implements OnInit {

  nombre = '';
  bio = '';
  biografia = '';
  tipo = '';
  img = '';

  peces: boolean;

  constructor(  private _PecesService: PecesService) { }

  ngOnInit(): void {
    }
  // tslint:disable-next-line:typedef

  nuevo() {
    this.peces = this._PecesService.add(this.nombre, this.bio, this.biografia, this.tipo, this.img);

  }


}
