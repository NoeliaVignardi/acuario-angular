import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarPezComponent } from './agregar-pez.component';

describe('AgregarPezComponent', () => {
  let component: AgregarPezComponent;
  let fixture: ComponentFixture<AgregarPezComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarPezComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarPezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
