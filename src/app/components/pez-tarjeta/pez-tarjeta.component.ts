import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router} from '@angular/router';
import { PecesService } from '../servicios/peces.service';
import { PecesComponent } from '../peces/peces.component';
import { Pez } from '../models/pez.model';

@Component({
  selector: 'app-pez-tarjeta',
  templateUrl: './pez-tarjeta.component.html'
})

export class PezTarjetaComponent implements OnInit {

  @Input() pez: any = {};
  @Input() index: number;

  @Output() pezSeleccionado: EventEmitter<number>;

  nombre = '';
  bio = '';
  biografia = '';
  tipo = '';
  img = '';

  peces: boolean;

  constructor(private _PecesService: PecesService, private router: Router) { 
    this.pezSeleccionado = new EventEmitter();
  }

  ngOnInit(): void {
  }

  verPez() {
    this.router.navigate( ['/pez', this.index] );
  }

  editFish(){
    this.router.navigate( ['/agregar']);  
    this.peces = this._PecesService.edit(this.nombre, this.bio, this.biografia, this.tipo, this.img);
    console.log(this.pez)
    }

  deleteFish(){
    this.peces = this._PecesService.delete(this.nombre, this.bio, this.biografia, this.tipo, this.img);
    this.router.navigate( ['/peces']);  
  }
  
}




