
import { identifierModuleUrl } from '@angular/compiler';
import { Injectable } from '@angular/core';

@Injectable()
export class PecesService {

    private peces:Pez[] = [
        {
          nombre: "Tetra Black Neon",
          bio: "Estos peces son de agua dulce, son de temperamento tranquilo...",
          biolarga: "Estos peces son de agua dulce, son de temperamento tranquilo, viven en temperaturas de 21º a 28ºC y llevan a cabo una dieta Omnívora.",
          img: "assets/img/tetra_clack_neon.jpg",
          tipo:"Agua Dulce"
        },
        {
          nombre: "Tetra Ember",
          bio: "Es un pez pequeño con la forma típica de los tetras que en estado adulto apenas alcanza los 2 a 2,5 cm de largo.",
          biolarga: "Es un pez pequeño con la forma típica de los tetras que en estado adulto apenas alcanza los 2 a 2,5 cm de largo. Es un pez omnívoro. Acepta de buen grado alimentos comerciales tanto en hojuelas como en granos así como también pasta.",
          img: "assets/img/tetra_ember.jpg",
          tipo:"Agua Dulce"
        },
        {
            nombre: "Carassius Surtidos",
            bio: "Carassius es un género de peces cipriniformes de la familia Cyprinidae.​",
            biolarga: "Carassius es un género de peces cipriniformes de la familia Cyprinidae.​ Son peces de agua dulce distribuidos originalmente en el este de Asia pero introducidos actualmente en gran parte de Europa.​",
            img: "assets/img/carassiud_surtidos.png",
            tipo:"Agua Dulce"
          },
        {
          nombre: "Espada Amarillo",
          bio: "Su cuerpo es largo con una sección transversal cilíndrica, y tiene la forma de un ",
          biolarga: "Su cuerpo es largo con una sección transversal cilíndrica, y tiene la forma de un palo largo. Tiene ojos muy grandes y dientes casi inexistentes que van desapareciendo mientras el pez crece. Tiene de 2 a 5 escamas con espinas, que también pierde mientras crece.",
          img: "assets/img/espada_amarillo.png",
          tipo:"Agua Dulce"
        },
        {
          nombre: "Espada Koi",
          bio: "Son peces generalmente pacíficos, (a veces algún macho se torna agresivo ) que conviven con otras especies.",
          biolarga: "Son peces generalmente pacíficos, (a veces algún macho se torna agresivo ) que conviven con otras especies. Es un pez muy pacífico y espabilado. El cola de espada es resistente, lo que quiere decir que se adapta bien al acuario comunitario, siempre y cuando la calidad de agua sea buena. Hay alguna rivalidad entre los machos, pero los cola de espada conviven bien con otras especies, aunque puede molestar a especies menores.",
          img: "assets/img/espada_koi.png",
          tipo:"Agua Dulce" 
        },
        {
          nombre: "Espada Negro",
          bio: "El pez xipho negro (espadas) es original de México en el que las temperaturas...",
          biolarga: "El pez xipho negro (espadas) es original de México en el que las temperaturas van desde los 15ºC a los 28ºC. Ríos y arroyos de corrientes suaves y aguas cristalinas.",
          img: "assets/img/espada_negro.png",
          tipo:"Agua Dulce"
        }
      ];

    constructor() {
        console.log("Servicio ok");
    }

    getPeces(): Pez[]{
        return this.peces;
    }

    getPez(  idx: string ){
      return this.peces[idx];
    }

    buscarPeces( termino:string ):Pez[]{
      let pecesArr:Pez[] = [];
      termino = termino.toLowerCase();
      for( let i = 0; i < this.peces.length; i ++ ){
        let pez = this.peces[i];
        let nombre = pez.nombre.toLowerCase();
        if( nombre.indexOf( termino ) >= 0  ){
          pez.idx = i;
          pecesArr.push( pez )
        }
      }
      return pecesArr;
    }

    add(nombre, bio, biolarga, tipo, img ): boolean {
      if (!nombre) { return false; }
      this.peces.push({nombre, bio, biolarga, tipo, img});
      console.log(this.peces)
      return true;
    }

    
    delete(nombre, bio, biolarga, tipo, img ): boolean {
      if (nombre) { 
        return false; }     
      else{
        this.peces.shift();
        console.log(this.peces)
        return true;
      }
    }

    edit(nombre, bio, biolarga, tipo, img ): boolean {
      if (!nombre) { 
        return false; }     
      else{
        this.peces.shift();
        console.log(this.peces)
        return true;
      }
    }

}

export interface Pez{
     nombre: string;
    bio: string;
   biolarga: string;
     img: string;
     tipo: string;
     idx?: number;
     //id: number;
 };