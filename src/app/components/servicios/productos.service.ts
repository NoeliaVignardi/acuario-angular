
import { Injectable } from '@angular/core';

@Injectable()
export class ProductosService {

    private productos:Producto[] = [
        {
          nombre: "Producto 1",
          bio: "Los peces tropicales son peces que habitan en medios ambientes tropicales.",
          biolarga: "Los peces tropicales son peces que habitan en medios ambientes tropicales en diversas partes del mundo, incluyendo tanto a especies de agua dulce como a especies de agua salada. Los peces tropicales son populares como peces para acuario, ya que muy a menudo poseen colores brillantes.",          
          img: "assets/img/producto-1.png",
          tipo:"Alimento"
        },
        {
            nombre: "Producto 2",
            bio: "Los peces tropicales son peces que habitan en medios ambientes tropicales.",
            biolarga: "Los peces tropicales son peces que habitan en medios ambientes tropicales en diversas partes del mundo, incluyendo tanto a especies de agua dulce como a especies de agua salada. Los peces tropicales son populares como peces para acuario, ya que muy a menudo poseen colores brillantes.",            
            img: "assets/img/producto-2.png",
            tipo:"Alimento"
        },
        {
            nombre: "Producto 3",
            bio: "Los peces tropicales son peces que habitan en medios ambientes tropicales.",
            biolarga: "Los peces tropicales son peces que habitan en medios ambientes tropicales en diversas partes del mundo, incluyendo tanto a especies de agua dulce como a especies de agua salada. Los peces tropicales son populares como peces para acuario, ya que muy a menudo poseen colores brillantes.",            
            img: "assets/img/producto-3.png",
            tipo:"Alimento"
          },
        {
            nombre: "Producto 4",
            bio: "Los peces tropicales son peces que habitan en medios ambientes tropicales.",
            biolarga: "Los peces tropicales son peces que habitan en medios ambientes tropicales en diversas partes del mundo, incluyendo tanto a especies de agua dulce como a especies de agua salada. Los peces tropicales son populares como peces para acuario, ya que muy a menudo poseen colores brillantes.",            
            img: "assets/img/producto-4.png",
            tipo:"Producto"
        },
        {
            nombre: "Producto 5",
            bio: "Los peces tropicales son peces que habitan en medios ambientes tropicales.",
            biolarga: "Los peces tropicales son peces que habitan en medios ambientes tropicales en diversas partes del mundo, incluyendo tanto a especies de agua dulce como a especies de agua salada. Los peces tropicales son populares como peces para acuario, ya que muy a menudo poseen colores brillantes.",            
            img: "assets/img/producto-5.png",
            tipo:"Producto"
        },
        {
            nombre: "Producto 6",
            bio: "Los peces tropicales son peces que habitan en medios ambientes tropicales.",
            biolarga: "Los peces tropicales son peces que habitan en medios ambientes tropicales en diversas partes del mundo, incluyendo tanto a especies de agua dulce como a especies de agua salada. Los peces tropicales son populares como peces para acuario, ya que muy a menudo poseen colores brillantes.",            
            img: "assets/img/producto-6.png",
            tipo:"Producto"
        }
      ];

    constructor() {
        console.log("Servicio ok");
    }

    getProductos(): Producto[]{
        return this.productos;
    }

    getProducto( idxx: string ){
      return this.productos[idxx];
    }

    buscarProductos( termino:string ):Producto[]{

      let productosArr:Producto[] = [];
      termino = termino.toLowerCase();
  
      for( let i = 0; i < this.productos.length; i ++ ){
  
        let producto = this.productos[i];
  
        let nombre = producto.nombre.toLowerCase();
  
        if( nombre.indexOf( termino ) >= 0  ){
          producto.idxx = i;
          productosArr.push( producto )
        }
  
      }
  
      return productosArr;
  
    }
}

export interface Producto{
    nombre: string;
    bio: string;
    biolarga: string;
    img: string;
    tipo: string;
    idxx?:number;
};