import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductosService } from '../servicios/productos.service';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html'
})
export class ProductoComponent {

  producto: any = {};

  constructor( private activatedRoute: ActivatedRoute,
               private productos_Service : ProductosService
    ) { 

    this.activatedRoute.params.subscribe( params =>{
      this.producto = this.productos_Service.getProducto( params['id']);
      console.log(this.producto);
    });
  }


}
