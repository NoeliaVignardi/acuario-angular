import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PecesService } from '../servicios/peces.service';


@Component({
  selector: 'app-pez',
  templateUrl: './pez.component.html'
})
export class PezComponent {

  pez: any = {};

  constructor( private activatedRoute: ActivatedRoute,
               private peces_Service : PecesService
    ) { 

    this.activatedRoute.params.subscribe( params =>{
      this.pez = this.peces_Service.getPez( params['id']);
      console.log(this.pez);
    });
  }


}
