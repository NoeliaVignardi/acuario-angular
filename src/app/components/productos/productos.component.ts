import { Component, OnInit } from '@angular/core';
import { ProductosService, Producto } from '../servicios/productos.service';
import { Router, RouterModule } from '@angular/router';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html'
})
export class ProductosComponent implements OnInit {

  productos: Producto[] = [];

  constructor( private productosService: ProductosService,
              private router: Router
               ) { 
  }

  ngOnInit() {
    this.productos = this.productosService.getProductos();
  }

  verProducto( idxx: number ){
    this.router.navigate( ['/producto',idxx] );
  }

}
