import { Component, OnInit } from '@angular/core';
import { PecesService} from '../servicios/peces.service';
import { Pez } from '../models/pez.model';
import { Router, RouterModule } from '@angular/router';


@Component({
  selector: 'app-peces',
  templateUrl: './peces.component.html'
})
export class PecesComponent implements OnInit {

  peces: Pez[] = [];

  constructor( private pecesService: PecesService,
              private router: Router
               ) { 
  // console.log("contructor");
  }

  ngOnInit() {
    this.peces = this.pecesService.getPeces();
    // console.log(this.peces);
  }

  verPez( idx: number ){
    this.router.navigate( ['/pez', idx] );
  }

}
