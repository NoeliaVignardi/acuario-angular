import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

//Rutas
import {APP_ROUTING} from './app.routes';

//Servicios
import { PecesService} from "./components/servicios/peces.service";
import { ProductosService} from "./components/servicios/productos.service";

//Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ProductoComponent } from './components/producto/producto.component';
import { PecesComponent } from './components/peces/peces.component';
import { PezComponent } from './components/pez/pez.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { PezTarjetaComponent } from './components/pez-tarjeta/pez-tarjeta.component';
import { ProductoTarjetaComponent } from './components/producto-tarjeta/producto-tarjeta.component';
import { LoginComponent } from './components/login/login.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { AgregarPezComponent } from './components/agregar-pez/agregar-pez.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ContactoComponent,
    ProductosComponent,
    PecesComponent,
    PezComponent,
    BuscadorComponent,
    PezTarjetaComponent,
    ProductoComponent,
    ProductoTarjetaComponent,
    LoginComponent,
    FooterComponent,
    AgregarPezComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    APP_ROUTING,
    FormsModule
  ],
  providers: [
    PecesService,
    ProductosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
