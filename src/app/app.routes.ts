import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ProductoComponent} from './components/producto/producto.component';
import { PecesComponent } from './components/peces/peces.component';
import { PezComponent} from './components/pez/pez.component';
import { BuscadorComponent} from './components/buscador/buscador.component';
import { LoginComponent } from './components/login/login.component';
import { AgregarPezComponent } from './components/agregar-pez/agregar-pez.component';


const APP_ROUTES: Routes = [
{ path: 'home', component: HomeComponent },
{ path: 'contacto', component: ContactoComponent},
{ path: 'productos', component: ProductosComponent},
{ path: 'producto', component: ProductoComponent},
{ path: 'producto/:id', component: ProductoComponent},
{ path: 'peces', component: PecesComponent},
{ path: 'pez/:id', component: PezComponent},
{ path: 'buscador/:termino', component: BuscadorComponent},
{ path: 'login', component: LoginComponent},
{ path: 'agregar', component: AgregarPezComponent},
{ path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true});
